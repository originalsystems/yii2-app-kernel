<?php

declare(strict_types=1);

namespace yii\bootstrap;

use yii\web\AssetBundle;

class BootstrapPluginAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/bootstrap/dist';

    public $js = [
        'js/bootstrap.bundle.min.js',
    ];

    public $depends = [
        \yii\web\JqueryAsset::class,
        \yii\bootstrap\BootstrapAsset::class,
    ];
}
