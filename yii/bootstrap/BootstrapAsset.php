<?php

declare(strict_types=1);

namespace yii\bootstrap;

use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/bootstrap/dist';

    public $css = [
        'css/bootstrap.min.css',
    ];
}
