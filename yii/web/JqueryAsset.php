<?php

declare(strict_types=1);

namespace yii\web;

class JqueryAsset extends \yii2kernel\web\AssetBundle
{
    public $sourcePath = '@node_modules/jquery/dist';
    public $js         = [
        'jquery.min.js',
    ];
}
