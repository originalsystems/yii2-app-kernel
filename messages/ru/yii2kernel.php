<?php

return [
    'Actions' => 'Действия',
    'An unexpected error occurred while data processing' => 'Возникла непредвиденная ошибка',
    'Cancel' => 'Отмена',
    'Click on preview for detailed view.' => 'Кликните на превью для детального просмотра',
    'Content type' => 'Тип данных',
    'Create' => 'Создать',
    'Created at' => 'Время создания',
    'Error' => 'Ошибка',
    'ID' => 'ID',
    'Original name' => 'Оригинальное название',
    'Path to file' => 'Путь до файла',
    'Record cannot be saved' => 'Запись не может быть сохранена',
    'Save' => 'Сохранить',
    'Specify one of values' => 'Выберите одно из значений',
    'Success!' => 'Успешно!',
    'The requested page does not exist.' => 'Запрошенная страница не существует.',
    'Transmitted information has been saved successfully.' => 'Передання информация успешно обработана',
    'Web uri address to file' => 'URI адрес до расположения файла',
];
