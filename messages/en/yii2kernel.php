<?php

return [
    'Actions' => 'Actions',
    'An unexpected error occurred while data processing' => 'An unexpected error occurred while data processing',
    'Cancel' => 'Cancel',
    'Click on preview for detailed view.' => 'Click on preview for detailed view.',
    'Content type' => 'Content type',
    'Create' => 'Create',
    'Created at' => 'Created at',
    'Error' => 'Error',
    'ID' => 'ID',
    'Original name' => 'Original name',
    'Path to file' => 'Path to file',
    'Record cannot be saved' => 'Record cannot be saved',
    'Save' => 'Save',
    'Specify one of values' => 'Specify one of values',
    'Success!' => 'Success!',
    'The requested page does not exist.' => 'The requested page does not exist.',
    'Transmitted information has been saved successfully.' => 'Transmitted information has been saved successfully.',
    'Web uri address to file' => 'Web uri address to file',
];
