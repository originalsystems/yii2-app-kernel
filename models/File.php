<?php

declare(strict_types=1);

namespace yii2kernel\models;

use yii\base\InvalidCallException;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2kernel\base\FileInterface;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $content_type
 * @property string $original_name
 * @property string $path
 * @property string $created_at
 *
 * @property string $thumbUrl
 * @property string $webUrl
 * @property string $realPath
 */
class File extends ActiveRecord implements FileInterface
{
    public const MIME_JPEG = 'image/jpeg';
    public const MIME_GIF  = 'image/gif';
    public const MIME_PNG  = 'image/png';
    public const MIME_SVG  = 'image/svg+xml';

    public static $imageMimeTypes = [
        self::MIME_JPEG,
        self::MIME_GIF,
        self::MIME_PNG,
        self::MIME_SVG,
    ];

    public static $resizeImageMimeTypes = [
        self::MIME_JPEG,
        self::MIME_PNG,
    ];

    public static function tableName(): string
    {
        return 'public.file';
    }

    public static function labels(): array
    {
        return [
            'id'            => \Yii::t('yii2kernel', 'ID'),
            'content_type'  => \Yii::t('yii2kernel', 'Content type'),
            'original_name' => \Yii::t('yii2kernel', 'Original name'),
            'path'          => \Yii::t('yii2kernel', 'Path to file'),
            'created_at'    => \Yii::t('yii2kernel', 'Created at'),
        ];
    }

    public static function upload(UploadedFile $file, string $prefix = null, bool $save = false): File
    {
        $path = self::uploadPath($file->getExtension(), $prefix);

        if ($file->saveAs($path) === false) {
            throw new InvalidCallException(__METHOD__ . ": can not upload file in path {$path}");
        }

        $model = new static([
            'content_type'  => $file->type,
            'original_name' => $file->name,
            'path'          => \str_replace(APP_ROOT, '@app', $path),
        ]);

        if ($save) {
            $model->strictSave();
        }

        return $model;
    }

    public static function byPath(string $source, string $prefix = null, bool $save = false): File
    {
        $file   = strpos($source, 'http') !== 0 ? \realpath(\Yii::getAlias($source)) : $source;
        $upload = \rtrim(\Yii::getAlias('@upload') . "/{$prefix}", '/');
        $path   = $file;

        if (\strpos($file, $upload) !== 0) {
            $path = self::uploadPath(\pathinfo($file, PATHINFO_EXTENSION), $prefix);

            \copy($file, $path);
        }

        $model = new static([
            'content_type'  => \mime_content_type($path),
            'original_name' => \basename($file),
            'path'          => \str_replace(APP_ROOT, '@app', $path),
        ]);

        if ($save) {
            $model->strictSave();
        }

        return $model;
    }

    public static function uploadPath(string $extension, string $prefix = null): string
    {
        $extension = \mb_strtolower($extension);

        do {
            $name = \rtrim(\md5((string) \random_int(0, PHP_INT_MAX)) . '.' . $extension, '.');
            $path = \preg_replace('#//+#', '/', \Yii::getAlias("@upload/{$prefix}/{$name[0]}/{$name[1]}{$name[2]}/{$name}"));
        } while (\file_exists($path));

        FileHelper::createDirectory(\dirname($path), \defined('DIR_MODE') ? DIR_MODE : 02775);

        return $path;
    }

    /**
     * Getters for thumb urls like:
     * - "thumbUrl140",      equals $file->getThumbUrl(140, 140)
     * - "thumbUrlW120",     equals $file->getThumbUrl(120, null)
     * - "thumbUrlH160",     equals $file->getThumbUrl(null, 160)
     * - "thumbUrlW120H160", equals $file->getThumbUrl(120, 160)
     *
     * @param string $name
     *
     * @return mixed|string
     * @throws \yii\base\InvalidParamException
     */
    public function __get($name)
    {
        if (\preg_match('#^thumbUrl(?P<size>\d+)$#', $name, $matches)) {
            $size = (int) $matches['size'];

            return $this->getThumbUrl($size, $size);
        }

        if (\preg_match('#^thumbUrl(?:W(?P<width>\d+))?(?:H(?P<height>\d+))?$#', $name, $matches)) {
            $width  = !empty($matches['width']) ? (int) $matches['width'] : null;
            $height = !empty($matches['height']) ? (int) $matches['height'] : null;

            return $this->getThumbUrl($width, $height);
        }

        return parent::__get($name);
    }

    public function getRealPath(): ?string
    {
        return \realpath(\Yii::getAlias($this->path)) ? : null;
    }

    public function getWebUrl(bool $absolute = false): ?string
    {
        $path    = $this->getRealPath();
        $webRoot = \Yii::getAlias('@webroot');

        if ($path === null && \strpos($path, $webRoot) !== 0) {
            return null;
        }

        return Url::to(\str_replace($webRoot, '', $path), $absolute);
    }

    public function getThumbUrl(int $width = null, int $height = null, bool $absolute = false): ?string
    {
        $uri = $this->getWebUrl();

        if ($uri === null) {
            return null;
        }

        if (in_array($this->content_type, self::$resizeImageMimeTypes, true) === false) {
            return null;
        }

        $prefix = "/thumb/{$width}x{$height}";

        if ($width === null && $height === null) {
            $prefix = '/thumb/160x-';
        } elseif ($width !== null && $height === null) {
            $prefix = "/thumb/{$width}x-";
        } elseif ($width === null && $height !== null) {
            $prefix = "/thumb/-x{$height}";
        }

        return Url::to($prefix . $uri, $absolute);
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules(): array
    {
        return [
            [['content_type', 'original_name', 'path'], 'required'],
            [['original_name'], 'string', 'max' => 256],
            [['content_type'], 'string', 'max' => 128],
            [['path'], 'string', 'max' => 512],
        ];
    }

    public function base64(): ?string
    {
        $file = $this->getRealPath();

        if ($file === null) {
            return null;
        }

        return \file_exists($file) && \is_readable($file) ? \base64_encode(\file_get_contents($file)) : null;
    }
}
