<?php

declare(strict_types=1);

namespace yii2kernel\base;

use yii\base\Model;
use yii2kernel\db\ActiveRecord;
use yii2kernel\mixin\ModelTrait;

abstract class FormModel extends Model
{
    use ModelTrait;

    abstract public function save(): bool;
}
