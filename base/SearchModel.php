<?php

declare(strict_types=1);

namespace yii2kernel\base;

use yii\data\DataProviderInterface;

interface SearchModel
{
    public function search(array $data = []): DataProviderInterface;
}
