<?php

declare(strict_types=1);

namespace yii2kernel\base;

interface FileInterface
{
    public function getRealPath(): ?string;

    public function getWebUrl(bool $absolute = false): ?string;

    public function getThumbUrl(int $width, int $height = null, bool $absolute = false): ?string;
}
