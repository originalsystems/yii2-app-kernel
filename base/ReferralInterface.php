<?php

declare(strict_types=1);

namespace yii2kernel\base;

interface ReferralInterface
{
    public function getReferralCode(): string;

    public static function findByReferralCode(string $code): ?ReferralInterface;

    public static function currentReferral(): ?ReferralInterface;
}
