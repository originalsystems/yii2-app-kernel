<?php

declare(strict_types=1);

namespace yii2kernel\behaviors;

use yii\base\InvalidConfigException;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

/**
 * @property \yii\db\BaseActiveRecord $owner
 */
class HashBehavior extends AttributeBehavior
{
    public $attribute = 'code';
    public $length    = 8;
    public $filter;

    public function init(): void
    {
        if (empty($this->attribute)) {
            throw new InvalidConfigException(__CLASS__ . '::$attribute can not be empty');
        }

        $this->attributes = [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => $this->attribute,
            BaseActiveRecord::EVENT_BEFORE_UPDATE   => $this->attribute,
            BaseActiveRecord::EVENT_BEFORE_INSERT   => $this->attribute,
        ];

        parent::init();
    }

    /**
     * @param \yii\base\Event $event
     *
     * @return string
     */
    protected function getValue($event): string
    {
        $owner = $this->owner;

        if ($owner->getAttribute($this->attribute) !== null) {
            return $owner->getAttribute($this->attribute);
        }

        do {
            $value = static::generateHash($this->length);
            $query = $owner::find();

            $query->andWhere([$this->attribute => $value]);

            if (is_callable($this->filter)) {
                call_user_func($this->filter, $query);
            }
        } while ($query->exists());

        return $value;
    }

    public static function generateHash(int $length): string
    {
        $bytes = random_bytes((int)ceil($length / 2));

        return substr(bin2hex($bytes), 0, $length);
    }
}
