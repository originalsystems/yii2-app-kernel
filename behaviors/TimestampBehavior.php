<?php

declare(strict_types=1);

namespace yii2kernel\behaviors;

use yii\db\BaseActiveRecord;
use yii\db\Expression;

/**
 * @property BaseActiveRecord $owner
 */
class TimestampBehavior extends \yii\behaviors\TimestampBehavior
{
    /**
     * @param BaseActiveRecord $owner
     */
    public function attach($owner): void
    {
        $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT] = [];
        $this->attributes[BaseActiveRecord::EVENT_BEFORE_UPDATE] = [];

        if ($owner->hasAttribute($this->createdAtAttribute)) {
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT][] = $this->createdAtAttribute;
        }

        if ($owner->hasAttribute($this->updatedAtAttribute)) {
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT][] = $this->updatedAtAttribute;
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_UPDATE][] = $this->updatedAtAttribute;
        }

        parent::attach($owner);
    }

    protected function getValue($event): Expression
    {
        return new Expression('NOW()');
    }
}
