<?php

declare(strict_types=1);

namespace yii2kernel\behaviors;

use yii\base\InvalidConfigException;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveQueryInterface;
use yii\db\BaseActiveRecord;
use yii\helpers\Inflector;

/**
 * @property \yii\db\BaseActiveRecord $owner
 */
class SlugBehavior extends AttributeBehavior
{
    public $to         = 'code';
    public $from       = 'name';
    public $max        = 64;
    public $filter;
    public $tailLength = 2;
    public $onUpdate   = true;

    public function init(): void
    {
        if (empty($this->to)) {
            throw new InvalidConfigException(__CLASS__ . '::$to should be name of attribute for generating slug');
        }

        if (empty($this->from)) {
            throw new InvalidConfigException(__CLASS__ . '::$from should be name of base attribute for generating slug');
        }

        if ($this->max < 8) {
            throw new InvalidConfigException(__CLASS__ . '::$max should not be less then 8');
        }

        $this->attributes = [
            BaseActiveRecord::EVENT_BEFORE_INSERT => $this->to,
        ];

        if ($this->onUpdate) {
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_UPDATE] = $this->to;
        }

        parent::init();
    }

    /**
     * @param \yii\base\Event $event
     *
     * @return string
     */
    protected function getValue($event): string
    {
        $owner = $this->owner;

        if ($owner->getIsNewRecord()) {
            if (!empty($owner->{$this->to})) {
                return $owner->{$this->to};
            }
        }

        if ($owner->getIsNewRecord() === false) {
            if (!empty($owner->{$this->to}) && $owner->isAttributeChanged($this->from) === false) {
                return $owner->{$this->to};
            }
        }

        $base = $this->generateSlug($owner->{$this->from});
        $slug = $base;

        if ($owner->{$this->to} === $slug) {
            return $owner->{$this->to};
        }

        $query = $this->getQuery($slug);

        while ($query->exists()) {
            $slug  = $base . $this->generateTail($this->tailLength);
            $query = $this->getQuery($slug);
        }

        return $slug;
    }

    protected function generateSlug(string $input = null): string
    {
        $output = $input ?? \Yii::$app->getSecurity()->generateRandomString($this->max - $this->tailLength);
        $output = \preg_replace('#[-_\.,]+#', '-', $output);
        $output = Inflector::slug($output, '-', true);
        $output = \mb_substr($output, 0, $this->max - ($this->tailLength + 1));

        return $output;
    }

    protected function getQuery(string $slug): ActiveQueryInterface
    {
        $query = $this->owner::find();

        $query->andWhere([$this->to => $slug]);

        if (\is_callable($this->filter)) {
            \call_user_func($this->filter, $query);
        }

        return $query;
    }

    protected function generateTail(int $length): string
    {
        $tail = '-';

        for ($i = $length; $i > 0; $i--) {
            /**
             * Generate symbol from ranges 48-57, 65-90, 97-122
             */

            $chr = \random_int(48, 109);

            if ($chr > 57) {
                $chr += 7;
            }

            if ($chr > 90) {
                $chr += 6;
            }

            $tail .= \chr($chr);
        }

        return $tail;
    }
}
