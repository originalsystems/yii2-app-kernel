<?php

declare(strict_types=1);

return [
    \yii\bootstrap\BootstrapAsset::class       => __DIR__ . '/yii/bootstrap/BootstrapAsset.php',
    \yii\bootstrap\BootstrapPluginAsset::class => __DIR__ . '/yii/bootstrap/BootstrapPluginAsset.php',
    \yii\helpers\Console::class                => __DIR__ . '/yii/helpers/Console.php',
    \yii\helpers\Html::class                   => __DIR__ . '/yii/helpers/Html.php',
    \yii\helpers\Url::class                    => __DIR__ . '/yii/helpers/Url.php',
    \yii\web\JqueryAsset::class                => __DIR__ . '/yii/web/JqueryAsset.php',
];
