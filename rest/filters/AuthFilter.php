<?php

namespace yii2kernel\rest\filters;

use yii\base\ActionFilter;
use yii\filters\auth\AuthInterface;
use yii\web\IdentityInterface;
use yii\web\UnauthorizedHttpException;

class AuthFilter extends ActionFilter implements AuthInterface
{
    public $tokenParam = 'access_token';

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        $user     = \Yii::$app->getUser();
        $request  = \Yii::$app->getRequest();
        $response = \Yii::$app->getResponse();

        try {
            $this->authenticate($user, $request, $response);
        } catch (UnauthorizedHttpException $e) {
        }

        return true;
    }

    public function authenticate($user, $request, $response): ?IdentityInterface
    {
        $token = $request->get($this->tokenParam);

        if (is_string($token)) {
            /**
             * @var \yii\web\IdentityInterface $class
             */
            $class    = $user->identityClass;
            $identity = $class::findIdentityByAccessToken($token, get_class($this));

            if ($identity !== null) {
                $user->setIdentity($identity);

                \Yii::$user = $identity;
            }
        }

        return $identity ?? \Yii::$user ?? null;
    }

    public function challenge($response)
    {
    }

    public function handleFailure($response)
    {
    }
}
