<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class CookieAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/jquery.cookie';

    public $js = [
        'jquery.cookie.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
