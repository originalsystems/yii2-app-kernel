<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class ReCaptchaAsset extends AssetBundle
{
    public $js = [
        'https://www.google.com/recaptcha/api.js',
    ];
}
