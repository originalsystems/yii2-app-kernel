<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class SimpleLineIconsAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/simple-line-icons';

    public $css = [
        'css/simple-line-icons.css',
    ];

    public function init()
    {
        parent:: init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            $dir  = \Yii::getAlias($this->sourcePath) . '/';
            $from = str_replace($dir, '', $from);

            return preg_match('#^(css|fonts)#', $from) === 1;
        };
    }
}
