<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;
use yii\web\JqueryAsset;

class FancyBoxAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/@fancyapps/fancybox/dist';

    public $js  = [
        'jquery.fancybox.min.js',
    ];
    public $css = [
        'jquery.fancybox.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
