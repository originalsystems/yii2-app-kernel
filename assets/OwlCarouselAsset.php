<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;
use yii\web\JqueryAsset;

class OwlCarouselAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/owl.carousel/dist';

    public $js  = [
        'owl.carousel.min.js',
    ];
    public $css = [
        'assets/owl.carousel.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
