<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class JqueryZoomAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/jquery-zoom';

    public $js = [
        'jquery.zoom.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
