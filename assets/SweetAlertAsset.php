<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class SweetAlertAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/sweetalert2/dist';

    public $js  = [
        'sweetalert2.min.js',
    ];
    public $css = [
        'sweetalert2.min.css',
    ];
}
