<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = '@node_modules/select2/dist';

    public $js = [
        'js/select2.min.js',
    ];

    public $css = [
        'css/select2.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
