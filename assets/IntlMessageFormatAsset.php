<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class IntlMessageFormatAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/intl-messageformat/dist';

    public $js = [
        'intl-messageformat-with-locales.min.js',
    ];
}
