<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class RemodalAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/remodal/dist';

    public $css = [
        'remodal.css',
        'remodal-default-theme.css',
    ];

    public $js = [
        'remodal.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
