<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class VuexAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/vuex/dist';

    public $js = [
        YII_ENV_PROD ? 'vuex.min.js' : 'vuex.js',
    ];

    public $depends = [
        VueAsset::class,
    ];
}
