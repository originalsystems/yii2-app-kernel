<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class NiceScrollAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/jquery.nicescroll/dist';

    public $js = [
        'jquery.nicescroll.min.js',
    ];
}
