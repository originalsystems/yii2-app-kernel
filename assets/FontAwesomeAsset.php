<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/font-awesome';

    public $css = [
        'css/font-awesome.min.css',
    ];

    public function init()
    {
        parent::init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('#/(fonts|css)#', $from);
        };
    }
}
