<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class MomentAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/moment/min';
    public $js         = [
        'moment-with-locales.min.js',
    ];
}
