<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class WaitMeAsset extends AssetBundle
{
    public $sourcePath = '@yii2kernel/assets/wait-me';

    public $js  = [
        'waitMe.min.js',
    ];
    public $css = [
        'waitMe.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
