<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class VueRouterAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/vue-router/dist';

    public $js = [
        YII_ENV_PROD ? 'vue-router.min.js' : 'vue-router.js',
    ];

    public $depends = [
        VueAsset::class,
    ];
}
