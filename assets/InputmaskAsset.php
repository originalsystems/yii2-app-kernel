<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\web\JqueryAsset;
use yii2kernel\web\AssetBundle;

class InputmaskAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/inputmask/dist';

    public $js = [
        'min/jquery.inputmask.bundle.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
