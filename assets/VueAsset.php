<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/vue/dist';

    public $js = [
        YII_ENV_PROD ? 'vue.min.js' : 'vue.js',
    ];
}
