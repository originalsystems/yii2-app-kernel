<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class VueSelectAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/vue-select/dist';

    public $js = [
        'vue-select.js',
    ];

    public $depends = [
        VueAsset::class,
    ];
}
