<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii\base\InvalidConfigException;
use yii2kernel\web\AssetBundle;

class FileDependAsset extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => YII_ENV_PROD === false,
    ];

    public function init()
    {
        if (empty($this->sourcePath)) {
            throw new InvalidConfigException(__CLASS__ . '::$sourcePath should not be empty');
        }

        $cID = \Yii::$app->controller->id;
        $aID = \Yii::$app->controller->action->id;

        $scripts = [
            'layout.js',
            "{$cID}.js",
            "{$cID}/{$aID}.js",
        ];

        $styles = [
            'layout.css',
            "{$cID}.css",
            "{$cID}/{$aID}.css",
        ];

        foreach ($scripts as $script) {
            $path = \Yii::getAlias($this->sourcePath . '/' . $script);

            if (file_exists($path)) {
                $this->js[] = $script;
            }
        }

        foreach ($styles as $style) {
            $path = \Yii::getAlias($this->sourcePath . '/' . $style);

            if (file_exists($path)) {
                $this->css[] = $style;
            }
        }
    }
}
