<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class SocketIOAsset extends AssetBundle
{
    public $sourcePath = '@node_modules/socket.io-client/dist';
    public $js         = [
        'socket.io.slim.js',
    ];
}
