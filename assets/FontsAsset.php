<?php

declare(strict_types=1);

namespace yii2kernel\assets;

use yii2kernel\web\AssetBundle;

class FontsAsset extends AssetBundle
{
    public $sourcePath = '@yii2kernel/assets/fonts';
    public $css        = [
        'fonts.css',
    ];
}
