<?php

declare(strict_types=1);

namespace yii2kernel\mixin;

/**
 * @property string      $type
 * @property-read string $typeLabel
 */
trait TypeLabel
{
    public static function typeLabels(): array
    {
        return [];
    }

    public function getTypeLabel(string $type = null): ?string
    {
        $labels = self::typeLabels();

        return $labels[$type ?? $this->type] ?? null;
    }
}
