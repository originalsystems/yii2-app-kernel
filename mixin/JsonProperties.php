<?php

declare(strict_types=1);

namespace yii2kernel\mixin;

use yii\base\InvalidConfigException;

/**
 * @method bool hasAttribute(string $name)
 */
trait JsonProperties
{
    /**
     * Model attribute name with json field
     */
    public static function jsonField(): string
    {
        return 'fields';
    }

    /**
     * Array of string with values, mapped in model
     */
    public static function jsonProperties(): array
    {
        return [];
    }

    /**
     * Validate jsonField() value, check it in model attributes
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function validateJsonProperties(): void
    {
        $jsonField = static::jsonField();

        if ($this->hasAttribute($jsonField) === false) {
            $className = \get_class($this);

            throw new InvalidConfigException("{$className} should contain attribute \${$jsonField}");
        }
    }

    /**
     * Get property from json attribute fields by name
     *
     * @param string     $property
     * @param mixed|null $default
     *
     * @return null
     * @throws \yii\base\InvalidConfigException
     */
    public function getJsonProperty(string $property, $default = null)
    {
        $this->validateJsonProperties();

        $jsonField = static::jsonField();

        return $this->{$jsonField}[$property] ?? $default;
    }

    /**
     * Set property to json attribute field, `null` value will unset setting field
     *
     * @param string     $property
     * @param mixed|null $value
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function setJsonProperty(string $property, $value = null): void
    {
        $this->validateJsonProperties();

        $jsonField = static::jsonField();
        $values    = $this->{$jsonField};

        if (\is_array($values) === false) {
            $values = [];
        }

        if ($value === null) {
            unset($values[$property]);
        } else {
            $values[$property] = $value;
        }

        $this->{$jsonField} = \count($values) !== 0 ? $values : null;
    }

    /**
     * @param string $name
     *
     * @return null
     * @throws \yii\base\InvalidConfigException
     */
    public function __get($name)
    {
        $this->validateJsonProperties();

        if (\in_array($name, static::jsonProperties(), true)) {
            return $this->getJsonProperty($name, null);
        }

        return parent::__get($name);
    }

    /**
     * @param string     $name
     * @param mixed|null $value
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function __set($name, $value)
    {
        $this->validateJsonProperties();

        if (\in_array($name, static::jsonProperties(), true)) {
            $this->setJsonProperty($name, $value);

            return;
        }

        parent::__set($name, $value);
    }

    /**
     * @param string $name
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function __isset($name)
    {
        $this->validateJsonProperties();

        if (\in_array($name, static::jsonProperties(), true)) {
            return $this->getJsonProperty($name) !== null;
        }

        return parent::__isset($name);
    }

    /**
     * @param string $name
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function __unset($name)
    {
        $this->validateJsonProperties();

        if (\in_array($name, static::jsonProperties(), true)) {
            $this->setJsonProperty($name, null);

            return;
        }

        parent::__unset($name);
    }
}
