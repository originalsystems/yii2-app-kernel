<?php

declare(strict_types=1);

namespace yii2kernel\mixin;

/**
 * @method array getErrors()
 */
trait ModelTrait
{
    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return static::labels();
    }

    /**
     * @return array
     */
    public static function labels(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function firstError(): ?string
    {
        $errors = $this->getErrors();
        $error  = null;

        \array_walk_recursive($errors, function ($input) use (&$error) {
            $error = $error ?? (\is_string($input) && !empty($input) ? $input : null);
        });

        return $error;
    }
}
