<?php

declare(strict_types=1);

namespace yii2kernel\mixin;

/**
 * @property string      $status
 * @property-read string $statusLabel
 */
trait StatusLabel
{
    public static function statusLabels(): array
    {
        return [];
    }

    public function getStatusLabel(string $status = null): ?string
    {
        $labels = self::statusLabels();

        return $labels[$status ?? $this->status] ?? null;
    }
}
