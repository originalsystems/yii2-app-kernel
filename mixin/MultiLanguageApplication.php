<?php

declare(strict_types=1);

namespace yii2kernel\mixin;

use yii\base\InvalidConfigException;

trait MultiLanguageApplication
{
    public $defaultLanguage;
    public $languages = [];

    public function initDefaultLanguage(): void
    {
        if (in_array($this->language, $this->languages, true) === false) {
            throw new InvalidConfigException('Default language should be is an available language');
        }

        $this->defaultLanguage = $this->language;
    }
}
