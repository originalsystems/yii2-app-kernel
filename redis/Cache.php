<?php

declare(strict_types=1);

namespace yii2kernel\redis;

class Cache extends \yii\redis\Cache
{
    public function buildKey($key): string
    {
        if (is_string($key) === false) {
            $key = md5(json_encode($key));
        }

        return $this->keyPrefix . $key;
    }

    protected function flushValues()
    {
        if (empty($this->keyPrefix)) {
            return $this->redis->flushdb();
        }

        return $this->redis->eval("return redis.call('del', unpack(redis.call('keys', ARGV[1])))", 0, $this->keyPrefix . '*');
    }
}
