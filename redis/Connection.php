<?php

declare(strict_types=1);

namespace yii2kernel\redis;

class Connection extends \yii\redis\Connection
{
    public function delByPattern(string $pattern)
    {
        $keys = $this->keys($pattern);

        foreach ($keys as $key) {
            $this->del($key);
        }
    }
}
