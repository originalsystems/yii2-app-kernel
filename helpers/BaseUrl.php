<?php

declare(strict_types=1);

namespace yii2kernel\helpers;

use Yii;

class BaseUrl extends \yii\helpers\BaseUrl
{
    /**
     * @param string|array $url
     * @param bool         $scheme
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public static function to($url = '', $scheme = false): string
    {
        $lang = Yii::$app->defaultLanguage ?? Yii::$app->language;

        if (is_array($url)) {
            if (isset($url['_lang'])) {
                if ($url['_lang'] === $lang) {
                    $url['_lang'] = null;
                }
            } elseif (Yii::$app->language !== $lang) {
                $url['_lang'] = Yii::$app->language;
            }
        }

        return parent::to($url, $scheme);
    }
}
