<?php

declare(strict_types=1);

namespace yii2kernel\helpers;

class Table
{
    public const TABLE_CLEAR_REGEX = '/[{}%"\']/';

    public static function clearTableName($table)
    {
        return preg_replace(self::TABLE_CLEAR_REGEX, '', $table);
    }

    public static function tableParts($table): ?array
    {
        $pair = explode('.', self::clearTableName($table));

        if (count($pair) === 1) {
            return ['public', $pair[0]];
        }

        if (count($pair) === 2) {
            return [$pair[0], $pair[1]];
        }

        return null;
    }
}
