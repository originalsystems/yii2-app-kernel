<?php

declare(strict_types=1);

namespace yii2kernel\helpers;

use Yii;
use yii\base\InvalidConfigException;
use yii\console\Exception;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii2kernel\helpers\Table;

class BaseConsole extends \yii\helpers\BaseConsole
{
    /**
     * @param string   $title
     * @param callable $callback
     * @param array    $params
     *
     * @return mixed
     */
    public static function timeout($title, $callback, array $params = [])
    {
        $start = microtime(true);

        echo " > start {$title}..." . PHP_EOL;

        $result = call_user_func_array($callback, $params);
        $time   = sprintf('%.3f', microtime(true) - $start) . ' seconds';

        if (Yii::$app->controller->isColorEnabled()) {
            $time = self::ansiFormat($time, [self::FG_GREEN]);
        }

        echo " > done {$title} in {$time}" . PHP_EOL;

        return $result;
    }
}
