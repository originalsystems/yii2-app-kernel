<?php

declare(strict_types=1);

namespace yii2kernel\helpers;

use Yii;
use yii2kernel\assets\FancyBoxAsset;
use yii2kernel\base\FileInterface;
use yii2kernel\web\Application;

class BaseHtml extends \yii\helpers\BaseHtml
{
    /**
     * Returns translated options for grid boolean filter or something else
     *
     * @return array
     */
    public static function boolLabels(): array
    {
        return [
            0 => Yii::t('yii', 'No'),
            1 => Yii::t('yii', 'Yes'),
        ];
    }

    /**
     * Check for link equals current uri.
     * If so, add rel=nofollow to A tag.
     *
     * @param string       $text
     * @param string|array $url
     * @param array        $options
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function a($text, $url = null, $options = []): ?string
    {
        if (Yii::$app instanceof Application) {
            if ($url !== null) {
                $url = $options['href'] = BaseUrl::to($url);
            }

            if (Yii::$app->getRequest()->getUrl() === $url) {
                $options['rel'] = 'nofollow';
            }

            return static::tag('a', $text, $options);
        }

        return parent::a($text, $url, $options);
    }

    public static function thumbPreview(FileInterface $image = null, $width = 160, array $options = []): ?string
    {
        if ($image === null) {
            return null;
        }

        FancyBoxAsset::register(Yii::$app->getView());

        $group                    = $options['group'] ?? true;
        $options['data-fancybox'] = $group;

        unset($options['group']);

        return self::a(self::img($image->getThumbUrl($width)), $image->getWebUrl(), $options);
    }

    public static function imageHint(FileInterface $image = null, $width = 160, array $options = []): ?string
    {
        if ($image === null) {
            return null;
        }

        $a = self::thumbPreview($image, $width, $options);

        if ($a === null) {
            return null;
        }

        return '<p>' . Yii::t('yii2kernel', 'Click on preview for detailed view.') . '</p>' . $a;
    }
}
