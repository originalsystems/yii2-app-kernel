<?php

declare(strict_types=1);

namespace yii2kernel\db;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @param string $query
     *
     * @return string
     */
    public static function tsQuery(string $query = null): ?string
    {
        if (empty($query)) {
            return null;
        }

        $parts = \array_filter(\preg_split('#[\s\(\)\.,;:]+#', \preg_replace('#[-|\$&!><\'"%]#', '', $query)));

        if (\count($parts) === 0) {
            return null;
        }

        $parts = \array_map(function (string $part) {
            return "{$part}:*";
        }, $parts);

        return \implode(' & ', $parts);
    }

    /**
     * @param \yii\db\Connection $db
     * @param string             $message
     *
     * @return array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function needOne($db = null, $message = 'The requested page does not exist.')
    {
        $model = $this->one($db);

        if ($model === null) {
            throw new NotFoundHttpException(Yii::t('yii2kernel', $message));
        }

        return $model;
    }

    /**
     * @param array $config
     *
     * @return \yii\data\DataProviderInterface
     */
    public function provider(array $config = []): DataProviderInterface
    {
        $config['query'] = $this;

        return new ActiveDataProvider($config);
    }

    /**
     * @param string|\Closure $from
     * @param string|\Closure $to
     * @param string|\Closure $group
     *
     * @return mixed
     */
    public function map($from, $to, $group = null): array
    {
        return ArrayHelper::map($this->all(), $from, $to, $group);
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return \yii2kernel\db\ActiveQuery
     */
    public function andFilterLike(string $attribute, string $value = null): ActiveQuery
    {
        return $this->andFilterWhere(['LIKE', "LOWER({$attribute})", $value !== null ? mb_strtolower($value) : null]);
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return \yii2kernel\db\ActiveQuery
     */
    public function andTsVectorSearchLike(string $attribute, string $value = null): ActiveQuery
    {
        static $i = 0;

        $tsQuery = self::tsQuery($value);

        if ($tsQuery === null) {
            return $this;
        }

        $param = ':SEARCH_TSVECTOR_TSQUERY_' . ($i++);

        return $this->andWhere("{$attribute} @@ TO_TSQUERY('russian', {$param})", [
            $param => $tsQuery,
        ]);
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return \yii2kernel\db\ActiveQuery
     */
    public function andSearchLike(string $attribute, string $value = null): ActiveQuery
    {
        return $this->andTsVectorSearchLike("TO_TSVECTOR('russian', {$attribute})", $value);
    }
}
