<?php

namespace yii2kernel\db\pgsql;

use yii\db\Schema;

class ColumnSchema extends \yii\db\pgsql\ColumnSchema
{
    public function phpTypecastValue($value)
    {
        switch ($this->type) {
            case Schema::TYPE_TIMESTAMP:
                return $value !== null ? \date('c', \strtotime($value)) : null;

            case Schema::TYPE_FLOAT:
            case Schema::TYPE_DOUBLE:
            case Schema::TYPE_DECIMAL:
                return $value !== null ? (float) $value : null;
        }

        return parent::phpTypecastValue($value);
    }
}
