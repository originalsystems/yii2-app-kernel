<?php

declare(strict_types=1);

namespace yii2kernel\db;

use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii2kernel\helpers\Table;
use yii2kernel\mixin\ModelTrait;

class ActiveRecord extends \yii\db\ActiveRecord
{
    use ModelTrait;

    /**
     * Returns kernel active query model instead of base framework object
     *
     * @return \yii2kernel\db\ActiveQuery|\common\db\ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(static::class);
    }

    /**
     * @param string    $message
     * @param bool|true $runValidation
     * @param array     $attributeNames
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function strictSave(string $message = null, bool $runValidation = true, $attributeNames = null): bool
    {
        $result = parent::save($runValidation, $attributeNames);

        if ($message === null) {
            $message = Yii::t('yii2kernel', 'Record cannot be saved');
        }

        if ($result === false) {
            $errors = $this->getFirstErrors();

            if (count($errors) > 0) {
                $message .= ', ' . current($errors);
            }

            throw new Exception($message);
        }

        return $result;
    }

    /**
     * @param mixed  $condition
     * @param string $message
     *
     * @return static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function needOne($condition, $message = null)
    {
        /**
         * @var ActiveRecord $result
         */
        $result = parent::findOne($condition);

        if ($message === null) {
            $message = Yii::t('yii2kernel', 'The requested page does not exist.');
        }

        if ($result === null) {
            throw new NotFoundHttpException(Yii::t('yii2kernel', $message));
        }

        return $result;
    }

    /**
     * Returns array of columns for current model table in format
     * - "table_name"."column_name_1"
     * - "table_name"."column_name_2"
     *
     * @param string $alias
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function tableColumns($alias = null): array
    {
        if ($alias === null) {
            [$schema, $alias] = Table::tableParts(static::tableName());
        }

        $columns = array_keys(static::getTableSchema()->columns);

        return array_map(function ($column) use ($alias) {
            return "\"{$alias}\".\"{$column}\"";
        }, $columns);
    }

    /**
     * Return human readable table i18n name
     * - "user" => "User"
     * - "offer_type" => "Offer type"
     *
     * @return string
     */
    public static function humanReadableName(): string
    {
        [$schema, $table] = Table::tableParts(self::tableName());

        return str_replace('_', ' ', ucfirst($table));
    }
}
