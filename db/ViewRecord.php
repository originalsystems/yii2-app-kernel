<?php

declare(strict_types=1);

namespace yii2kernel\db;

use yii\base\InvalidCallException;
use yii2kernel\helpers\Table;

class ViewRecord extends ActiveRecord
{
    /**
     * @throws \yii\db\Exception
     */
    public static function refreshView(): void
    {
        [$schema, $table] = Table::tableParts(static::tableName());

        \Yii::$app->getDb()->createCommand("REFRESH MATERIALIZED VIEW \"{$schema}\".\"{$table}\"")->execute();
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool|void
     * @throws \yii\base\InvalidCallException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        throw new InvalidCallException('Current model based on database  view and can not be saved');
    }
}
