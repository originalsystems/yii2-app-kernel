<?php

declare(strict_types=1);

namespace yii2kernel\web;

use Yii;

class Response extends \yii\web\Response
{
    public function cache(int $ttl = null): void
    {
        if ($ttl === null || $ttl <= 0) {
            $this->getHeaders()->set('Cache-Control', 'no-store, no-cache, must-revalidate');
            $this->getHeaders()->set('Pragma', 'no-cache');
            $this->getHeaders()->set('Expires', \gmdate('D, d M Y H:i:s T', 0));
        } else {
            $this->getHeaders()->set('Cache-Control', 'public');
            $this->getHeaders()->set('Pragma', 'public');
            $this->getHeaders()->set('Expires', \gmdate('D, d M Y H:i:s T', \time() + 3600));
        }
    }

    public function postRedirect(string $url, array $data = []): \yii\web\Response
    {
        $this->format = self::FORMAT_HTML;
        $this->data   = Yii::$app->controller->renderPartial('@yii2kernel/web/views/redirect', [
            'url'  => $url,
            'data' => $this->plainData($data),
        ]);

        return $this;
    }

    private function plainData(array $data = [], $prefix = null): array
    {
        $result = [];

        foreach ($data as $name => $value) {
            $name = $prefix === null ? $name : "{$prefix}[{$name}]";

            if (\is_scalar($value)) {
                $result[$name] = $value;
            } elseif (\is_array($value)) {
                $result += $this->plainData($value, $name);
            }
        }

        return $result;
    }
}
