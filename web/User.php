<?php

declare(strict_types=1);

namespace yii2kernel\web;

use yii\base\BootstrapInterface;

/**
 * Class User
 *
 * @package yii2kernel\web
 * @method \common\models\User getIdentity($autoRenew = true)
 * @property \common\models\User $identity
 */
class User extends \yii\web\User implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        \Yii::$user = $this->getIdentity();
    }
}
