<?php

/**
 * @var array  $data
 * @var string $url
 * @var string $method
 */

declare(strict_types=1);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Javascript redirect</title>
</head>
<body>
<form action="<?= $url; ?>" method="POST" name="redirect">
    <?php foreach ($data as $key => $value): ?>
        <input type="hidden" name="<?= $key; ?>" value="<?= $value; ?>">
    <?php endforeach; ?>
</form>
<script type="text/javascript">
    document.redirect.submit();
</script>
</body>
</html>
