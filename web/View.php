<?php

declare(strict_types=1);

namespace yii2kernel\web;

use common\web\AssetBundle;
use Yii;
use yii\base\Application;

/**
 * Class View
 *
 * @package yii2kernel\web
 * @property string $description
 * @property string $keywords
 */
class View extends \yii\web\View
{
    public $breadcrumbs = [];

    /**
     * @param string $url
     * @param array  $options
     * @param string $key
     *
     * @throws \yii\base\InvalidParamException
     */
    public function registerJsFile($url, $options = [], $key = null)
    {
        if (Yii::$app->getAssetManager()->appendTimestamp === true) {
            $url = $this->appendTimestamp($url);
        }

        parent::registerJsFile($url, $options, $key);
    }

    /**
     * @param string $url
     * @param array  $options
     * @param string $key
     *
     * @throws \yii\base\InvalidParamException
     */
    public function registerCssFile($url, $options = [], $key = null)
    {
        if (Yii::$app->getAssetManager()->appendTimestamp === true) {
            $url = $this->appendTimestamp($url);
        }

        parent::registerCssFile($url, $options, $key);
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function appendTimestamp($file): string
    {
        if (strpos($file, '/') === 0) {
            $path = Yii::getAlias('@webroot') . $file;

            if (file_exists($path) && ($timestamp = @filemtime($path)) > 0) {
                return $file . (strpos($file, '?') === false ? '?' : '&') . 'v=' . $timestamp;
            }
        }

        return $file;
    }

    /**
     * @var string|null
     */
    private $_description;

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->_description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->registerMetaTag(['name' => 'description', 'content' => $description], 'page-description');
        $this->_description = $description;
    }

    /**
     * @var string|null
     */
    private $_keywords;

    /**
     * @return string|null
     */
    public function getKeywords(): ?string
    {
        return $this->_keywords;
    }

    /**
     * @param string|array $keywords
     */
    public function setKeywords($keywords)
    {
        if (is_array($keywords)) {
            $keywords = implode(', ', $keywords);
        }

        $this->registerMetaTag(['name' => 'keywords', 'content' => $keywords], 'page-keywords');
        $this->_keywords = $keywords;
    }
}
