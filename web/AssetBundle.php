<?php

declare(strict_types=1);

namespace yii2kernel\web;

use Yii;

class AssetBundle extends \yii\web\AssetBundle
{
    public function init()
    {
        parent::init();

        if (Yii::$app->getAssetManager()->appendTimestamp === true) {
            $this->js  = array_map([Yii::$app->getView(), 'appendTimestamp'], $this->js);
            $this->css = array_map([Yii::$app->getView(), 'appendTimestamp'], $this->css);
        }
    }
}
