<?php

declare(strict_types=1);

namespace yii2kernel\web\action\json;

use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class View extends \yii\base\Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var array
     */
    public $fields = [];

    public function init()
    {
        if (is_string($this->modelClass) === false) {
            throw new InvalidConfigException(__CLASS__ . '::$modelClass should configured with active record model class name');
        }

        if (class_exists($this->modelClass) === false) {
            throw new InvalidConfigException(__CLASS__ . '::$modelClass should configured with active record model class name');
        }

        if (in_array(\yii\db\ActiveRecord::class, class_parents($this->modelClass), true) === false) {
            throw new InvalidConfigException(__CLASS__ . '::$modelClass should configured with active record model class name');
        }

        if (empty($this->fields)) {
            throw new InvalidConfigException(__CLASS__ . '::$fields should configured with string map of request and active record fields');
        }

        $fields = [];

        foreach ((array)$this->fields as $requestField => $modelField) {
            if (is_string($modelField) === false) {
                throw new InvalidConfigException(__CLASS__ . '::$fields should configured with string map of request and active record fields');
            }

            $requestField = is_numeric($requestField) ? $modelField : $requestField;

            $fields[$requestField] = $modelField;
        }

        $this->fields = $fields;
    }

    public function run()
    {
        $request  = \Yii::$app->getRequest();
        $response = \Yii::$app->getResponse();

        $response->format = Response::FORMAT_JSON;

        /**
         * @var \yii\db\ActiveRecord $modelClass
         * @var \yii\db\ActiveQuery  $query
         */

        $modelClass = $this->modelClass;
        $query      = $modelClass::find();

        foreach ($this->fields as $requestField => $modelField) {
            $requestValue = $request->getBodyParam($requestField, $request->getQueryParam($requestField));

            if ($requestValue === null || is_array($requestValue)) {
                throw new BadRequestHttpException(\Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                    'param' => $requestField,
                ]));
            }

            $query->andWhere([$modelField => $requestValue]);
        }

        return $query->one();
    }
}
