<?php

declare(strict_types=1);

namespace yii2kernel\web;

use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;

class Controller extends \yii\web\Controller
{
    protected function ajaxValidation(Model $model, string $formName = null)
    {
        $req = Yii::$app->getRequest();
        $res = Yii::$app->getResponse();

        if ($req->getIsAjax() && $req->getIsPjax() === false && $model->load($req->post(), $formName)) {
            $model->load($req->post());

            $res->format = Response::FORMAT_JSON;
            $res->data   = ActiveForm::validate($model);

            Yii::$app->end(0, $res);
        }
    }

    /**
     * Disables debug module panels
     */
    protected function disableDebugPanels(): void
    {
        if (empty(Yii::$app->modules['debug'])) {
            return;
        }

        /**
         * @var \yii\debug\Module $debug
         */
        $debug = Yii::$app->modules['debug'];

        Yii::$app->getView()->off(\yii\web\View::EVENT_END_BODY, [$debug, 'renderToolbar']);
        Yii::$app->getResponse()->off(\yii\web\Response::EVENT_AFTER_PREPARE, [$debug, 'setDebugHeaders']);
    }

    /**
     * @param array             $data
     * @param string|array|null $errors
     *
     * @return \yii\web\Response
     */
    public function json(array $data = [], $errors = null): \yii\web\Response
    {
        $errors = (array) $errors;
        $error  = null;

        array_walk_recursive($errors, function ($input) use (&$error) {
            $error = $error ?? ($input ? : null);
        });

        return $this->asJson([
            'code'  => $error === null ? 0 : -1,
            'data'  => $data,
            'error' => $error,
        ]);
    }

    /**
     * @param \yii\base\Model $model
     *
     * @return \yii\web\Response
     */
    public function jsonByModel(Model $model): \yii\web\Response
    {
        return $this->json($model->getAttributes(), $model->getErrors());
    }
}
