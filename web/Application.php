<?php

declare(strict_types=1);

namespace yii2kernel\web;

use yii\base\Action;
use yii2kernel\mixin\MultiLanguageApplication;

/**
 * Class Application
 *
 * @package yii2kernel\web
 * @property \yii\redis\Cache             $cache
 * @property \yii\redis\Connection        $redis
 * @property \yii2kernel\rbac\AuthManager $authManager
 * @property \yii2kernel\web\AssetManager $assetManager
 * @property \yii2kernel\web\Controller   $controller
 * @property \yii2kernel\web\ErrorHandler $errorHandler
 * @property \yii2kernel\web\User         $user
 * @property \yii2kernel\web\View         $view
 * @property \yii2kernel\web\Referral     $referral
 * @property \yii2kernel\web\Request      $request
 * @property \yii2kernel\web\Response     $response
 *
 * @method \yii\redis\Cache             getCache()
 * @method \yii2kernel\rbac\AuthManager getAuthManager()
 * @method \yii2kernel\web\AssetManager getAssetManager()
 * @method \yii2kernel\web\ErrorHandler getErrorHandler()
 * @method \yii2kernel\web\User         getUser()
 * @method \yii2kernel\web\View         getView()
 * @method \yii2kernel\web\Request      getRequest()
 * @method \yii2kernel\web\Response     getResponse()
 */
class Application extends \yii\web\Application
{
    use MultiLanguageApplication;

    public function init(): void
    {
        $this->initDefaultLanguage();

        parent::init();
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action): bool
    {
        $this->languageSettingUp($action);

        return parent::beforeAction($action);
    }

    private function languageSettingUp(Action $action): void
    {
        if ($action->id === 'error') {
            $this->language = $this->defaultLanguage;

            return;
        }

        $lang = $this->getRequest()->get('_lang');

        if ($lang === null) {
            return;
        }

        if ($lang === $this->defaultLanguage || in_array($lang, $this->languages, true) === false) {
            $params = $this->getRequest()->getQueryParams();

            unset($params['_lang']);

            $this->getResponse()->redirect([null] + $params);
            $this->end();
        }

        $this->language = $lang;
    }
}
