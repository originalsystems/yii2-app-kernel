<?php

declare(strict_types=1);

namespace yii2kernel\web;

class ErrorHandler extends \yii\web\ErrorHandler
{
    public $backUrl = ['/'];
}
