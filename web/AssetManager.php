<?php

declare(strict_types=1);

namespace yii2kernel\web;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;

class AssetManager extends \yii\web\AssetManager
{
    /**
     * Compress all registered local js files into one minified file via node-uglifyjs
     *
     * @var bool
     */
    public $compressJs = false;

    /**
     * Compress all registered local css files into one minified file via node-cleancss
     *
     * @var bool
     */
    public $compressCss = false;

    public function init(): void
    {
        parent::init();

        $this->compressFiles();
    }

    private function compressFiles(): void
    {
        if ($this->compressCss !== true && $this->compressJs !== true) {
            return;
        }

        if (class_exists(JS::class) === false || class_exists(CSS::class) === false) {
            return;
        }

        Yii::$app->view->on(View::EVENT_END_PAGE, function () {
            $view = Yii::$app->view;

            if ($this->compressJs === true) {
                if (!empty($view->jsFiles[View::POS_HEAD])) {
                    $view->jsFiles[View::POS_HEAD] = $this->compressJsFiles($view->jsFiles[View::POS_HEAD]);
                }

                if (!empty($view->jsFiles[View::POS_BEGIN])) {
                    $view->jsFiles[View::POS_BEGIN] = $this->compressJsFiles($view->jsFiles[View::POS_BEGIN]);
                }

                if (!empty($view->jsFiles[View::POS_END])) {
                    $view->jsFiles[View::POS_END] = $this->compressJsFiles($view->jsFiles[View::POS_END]);
                }
            }

            if ($this->compressCss === true) {
                if (!empty($view->cssFiles)) {
                    $view->cssFiles = $this->compressCssFiles($view->cssFiles);
                }
            }
        });
    }

    private function compressJsFiles(array $files): array
    {
        if (class_exists(JS::class) === false) {
            return $files;
        }

        $web    = Yii::getAlias('@webroot');
        $local  = [];
        $result = [];

        /**
         * Separate local files from remote
         */
        foreach ($files as $file => $tag) {
            $clearFile = explode('?', $file)[0];
            $path      = $web . $clearFile;

            if (strpos($file, '/') === 0 && file_exists($path) && ($ts = filemtime($path)) > 0) {
                $local[$path] = $ts;
            } else {
                $result[$file] = $tag;
            }
        }

        if (count($local) === 0) {
            return $result;
        }

        $hash = md5(serialize($local));
        $uri  = "/assets/minify/js/{$hash[0]}/{$hash[1]}{$hash[2]}/{$hash}.js";
        $path = $web . $uri;

        FileHelper::createDirectory(dirname($path), 02775);

        /**
         * If minify version exists - return it
         */
        if (file_exists($path)) {
            $result[$uri] = Html::jsFile($uri);

            return $result;
        }

        /**
         * Register minify generator after send response
         */
        $lock = __METHOD__ . '::' . $hash;

        if (Yii::$app->getCache()->get($lock) === false) {
            Yii::$app->getCache()->set($lock, true, 300);
            Yii::$app->getResponse()->on(Response::EVENT_AFTER_SEND, function () use ($local, $path) {
                fastcgi_finish_request();

                (new JS(array_keys($local)))->minify($path);
            });
        }

        return $files;
    }

    private function compressCssFiles(array $files): array
    {
        if (class_exists(CSS::class) === false) {
            return $files;
        }

        $web    = Yii::getAlias('@webroot');
        $local  = [];
        $result = [];

        foreach ($files as $file => $tag) {
            $clearFile = explode('?', $file)[0];
            $path      = $web . $clearFile;

            if (strpos($file, '/') === 0 && file_exists($path) && ($ts = filemtime($path)) > 0) {
                $local[$path] = $ts;
            } else {
                $result[$file] = $tag;
            }
        }

        if (count($local) === 0) {
            return $result;
        }

        $hash = md5(serialize($local));
        $uri  = "/assets/minify/css/{$hash[0]}/{$hash[1]}{$hash[2]}/{$hash}.css";
        $path = $web . $uri;

        FileHelper::createDirectory(dirname($path), 02775);

        /**
         * If minify version exists - return it
         */
        if (file_exists($path)) {
            $result[$uri] = Html::cssFile($uri);

            return $result;
        }

        /**
         * Register minify generator after send response
         */
        $lock = __METHOD__ . '::' . $hash;

        if (Yii::$app->getCache()->get($lock) === false) {
            Yii::$app->getCache()->set($lock, true, 300);
            Yii::$app->getResponse()->on(Response::EVENT_AFTER_SEND, function () use ($local, $path) {
                fastcgi_finish_request();

                (new CSS(array_keys($local)))->minify($path);
            });
        }

        return $files;
    }
}
