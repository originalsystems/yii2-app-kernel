<?php

declare(strict_types=1);

namespace yii2kernel\web;

use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\Cookie;
use yii2kernel\base\ReferralInterface;

class Referral extends Component
{
    /**
     * @var string
     */
    public $name = 'rid';

    /**
     * @var string|array
     */
    public $baseUrl = ['/'];

    /**
     * @var ReferralInterface
     */
    public $modelClass;

    public function init(): void
    {
        if (is_string($this->modelClass) === false) {
            throw new InvalidConfigException(__CLASS__ . ': $modelClass should be configured with class name of ' . ReferralInterface::class . ' model');
        }

        if (class_exists($this->modelClass) === false) {
            throw new InvalidConfigException(__CLASS__ . ': $modelClass should contain existing class name');
        }

        if (in_array(ReferralInterface::class, class_implements($this->modelClass), true) === false) {
            throw new InvalidConfigException(__CLASS__ . ': $modelClass should implements ' . ReferralInterface::class . ' interface');
        }
    }

    public function get(): ?ReferralInterface
    {
        $cookieModel  = null;
        $requestModel = null;

        if (($cookie = \Yii::$app->getRequest()->getCookies()->get($this->name)) && is_string($cookie->value)) {
            $cookieModel = $this->modelClass::findByReferralCode($cookie->value);
        }

        if (($code = \Yii::$app->getRequest()->getQueryParam($this->name)) && is_string($code)) {
            $requestModel = $this->modelClass::findByReferralCode($code);
        }

        return $requestModel ?? $cookieModel ?? null;
    }

    public function set(ReferralInterface $referral): void
    {
        \Yii::$app->getResponse()->getCookies()->add(new Cookie([
            'name'   => $this->name,
            'value'  => $referral->getReferralCode(),
            'expire' => time() + 7776000,
        ]));
    }

    public function clean(): void
    {
        \Yii::$app->getResponse()->getCookies()->add(new Cookie([
            'name'   => $this->name,
            'expire' => 1,
        ]));
    }

    public function resolve(): void
    {
        $params = \Yii::$app->getRequest()->getQueryParams();

        if (isset($params[$this->name]) === false || is_string($params[$this->name]) === false) {
            return;
        }

        $referral = $this->modelClass::findByReferralCode($params[$this->name]);

        if ($referral !== null) {
            $this->set($referral);
        }

        if (\Yii::$app->getRequest()->getMethod() === 'GET') {
            unset($params[$this->name]);

            \Yii::$app->getResponse()->redirect(\yii\helpers\Url::to([null] + $params));

            \Yii::$app->end();
        }
    }

    public function url(ReferralInterface $referral = null): ?string
    {
        if ($referral === null) {
            return null;
        }

        $url              = (array)$this->baseUrl;
        $url[$this->name] = $referral->getReferralCode();

        return Url::to($url, true);
    }

    public function currentUrl(): ?string
    {
        return $this->url($this->modelClass::currentReferral());
    }
}
