<?php

declare(strict_types=1);

namespace yii2kernel\widgets;

use yii\base\Widget;
use yii2kernel\assets\SweetAlertAsset;

class Message extends Widget
{
    public const TYPE_SUCCESS  = 'success';
    public const TYPE_ERROR    = 'error';
    public const TYPE_WARNING  = 'warning';
    public const TYPE_INFO     = 'info';
    public const TYPE_QUESTION = 'question';

    public static $types = [
        self::TYPE_SUCCESS,
        self::TYPE_ERROR,
        self::TYPE_WARNING,
        self::TYPE_INFO,
        self::TYPE_QUESTION,
    ];

    public function run()
    {
        SweetAlertAsset::register($this->getView());

        $session = \Yii::$app->getSession();
        $message = $session->getFlash('message', []);

        $text  = null;
        $title = null;
        $type  = null;

        if (is_array($message)) {
            $text  = $message[0] ?? null;
            $title = $message[1] ?? null;
            $type  = $message[2] ?? null;
        } elseif (is_string($message)) {
            $text  = $message;
            $title = null;
            $type  = self::TYPE_SUCCESS;
        }

        if (empty($title)) {
            return;
        }

        if (in_array($type, self::$types, true) === false) {
            $type = null;
        }

        $options = [
            'text'  => $text,
            'title' => $title,
            'type'  => $type,
        ];

        $options = json_encode($options);

        \Yii::$app->getView()->registerJs("swal({$options});");
    }

    /**
     * @param string $text
     * @param string $title
     * @param string $type
     */
    public static function send($title, $text = '', $type = self::TYPE_SUCCESS): void
    {
        \Yii::$app->getSession()->setFlash('message', [$text, $title, $type]);
    }
}
