<?php

/**
 * @var \common\web\View                           $this
 * @var \yii2kernel\gii\generators\model\Generator $generator
 * @var string                                     $tableName
 * @var string                                     $className
 * @var string                                     $queryClassName
 * @var \yii\db\TableSchema                        $tableSchema
 * @var string[]                                   $labels
 * @var string[]                                   $rules
 * @var array                                      $relations
 */

declare(strict_types=1);

$genProperties = [];
$genRelations  = [];
$hasBehaviors  = false;
$hasTimes      = false;
$jsonMap       = [];

foreach ((array)$tableSchema->columns as $column) {
    if ($column->name === 'created_at' || $column->name === 'updated_at') {
        $hasTimes     = true;
        $hasBehaviors = true;
    }

    if (stripos($column->dbType, 'json') === 0) {
        if ($column->name === 'fields') {
            $jsonMap[$column->name] = 'json';
        } else {
            $jsonMap[$column->name] = $column->name . '_array';
        }
    }

    $colType = $column->phpType;

    switch ($colType) {
        case 'integer':
            $colType = 'int';
            break;

        case 'boolean':
            $colType = 'bool';
            break;
    }

    $genProperties[] = [
        'name'     => $column->name,
        'type'     => $colType,
        'relation' => false,
    ];
}

foreach ($relations as $name => [$relExpression, $relName, $relMany]) {
    if (stripos($relName, 'xref') !== false) {
        continue;
    }

    $genProperties[] = [
        'name'     => $name,
        'type'     => $relMany === true ? $relName . '[]' : $relName,
        'relation' => true,
    ];

    $genRelations[] = [
        'name' => $name,
        'rel'  => $relExpression,
    ];
}
?>
<?= '<?php' . PHP_EOL; ?>

declare(strict_types = 1);

namespace <?= $generator->ns; ?>;

<?= count($jsonMap) > 0 ? 'use yii2kernel\behaviors\JsonBehavior;' . PHP_EOL : ''; ?>
<?= $hasTimes === true ? 'use yii2kernel\behaviors\TimestampBehavior;' . PHP_EOL : ''; ?>
use common\db\ActiveRecord;
use common\db\ActiveQuery;

/**
<?php
$prevProperty = null;

foreach ($genProperties as $property) {
    if ($prevProperty !== null && $prevProperty['relation'] === false && $property['relation'] === true) {
        echo ' *' . PHP_EOL;
    }

    echo ' * @property ' . $property['type'] . ' $' . lcfirst($property['name']) . PHP_EOL;

    $prevProperty = $property;
}

if (count($jsonMap) > 0) {
    $hasBehaviors = true;
    echo ' *' . PHP_EOL;
}

foreach ($jsonMap as $attribute => $property) {
    echo ' * @property array $' . $property . PHP_EOL;
}
?>
*/
class <?= $className; ?> extends ActiveRecord
{
<?php foreach ($jsonMap as $attribute => $property) {
    echo PHP_EOL . '    public $' . $property . ' = [];';
} ?>

public static function tableName(): string
{
return '<?= $tableSchema->schemaName . '.' . $tableSchema->name; ?>';
}
<?php if ($generator->db !== 'db'): ?>
    public static function getDb()
    {
    return Yii::$app->get('<?= $generator->db; ?>');
    }
<?php endif; ?>

<?php if ($hasBehaviors === true): ?>
    public function behaviors(): array
    {
    return [
    <?php if ($hasTimes): ?><?= 'TimestampBehavior::class,' . PHP_EOL; ?><?php endif; ?>
    <?php if (count($jsonMap) > 0): ?>[
        'class' => JsonBehavior::class,
        <?= "'map' => " . \yii\helpers\VarDumper::dumpAsString($jsonMap); ?>
        ],
    <?php endif; ?>
    ];}
<?php endif; ?>

public function rules(): array
{
return [
<?= implode(',' . PHP_EOL, $rules) . ','; ?>
];
}

public static function labels(): array
{
return [
<?php foreach ($labels as $name => $label) {
    echo "'$name' => " . $generator->generateString($label) . ',' . PHP_EOL;
}
?>
];
}

<?php foreach ($genRelations as $relation): ?>
    public function get<?= $relation['name']; ?>(): ActiveQuery
    {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    <?= $relation['rel'] . PHP_EOL; ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName): ?>
    <?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    ?>
    /** @noinspection PhpHierarchyChecksInspection */
    public static function find(): \yii\db\ActiveQuery
    {
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
}
