<?php

/**
 * @var \common\web\View                           $this
 * @var \yii2kernel\gii\generators\model\Generator $generator
 * @var string                                     $tableName
 * @var string                                     $className
 * @var yii\db\TableSchema                         $tableSchema
 * @var string[]                                   $labels
 * @var string[]                                   $rules
 * @var array                                      $relations
 * @var string                                     $className
 * @var string                                     $modelClassName
 */

declare(strict_types=1);

$modelFullClassName = $modelClassName;
if ($generator->ns !== $generator->queryNs) {
    $modelFullClassName = '\\' . $generator->ns . '\\' . $modelFullClassName;
}

echo "<?php\n";
?>

declare(strict_types=1);

namespace <?= $generator->queryNs ?>;

/**
* @see <?= $modelFullClassName . "\n" ?>
* @method <?= $modelFullClassName; ?>|array|null one($db = null)
* @method <?= $modelFullClassName; ?>[]|array many($db = null)
*/
class <?= $className ?> extends <?= '\\' . ltrim($generator->queryBaseClass, '\\') . "\n" ?>
{
}
