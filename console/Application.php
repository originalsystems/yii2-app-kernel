<?php

declare(strict_types=1);

namespace yii2kernel\console;

use yii\console\controllers\AssetController;
use yii\console\controllers\CacheController;
use yii\console\controllers\FixtureController;
use yii\console\controllers\MessageController;
use yii\console\controllers\ServeController;
use yii\console\controllers\MigrateController;
use yii2kernel\console\controllers\HelpController;
use yii2kernel\console\controllers\I18nController;
use yii2kernel\mixin\MultiLanguageApplication;

class Application extends \yii\console\Application
{
    use MultiLanguageApplication;

    public function init()
    {
        $this->initDefaultLanguage();

        parent::init();
    }

    public function coreCommands(): array
    {
        return [
            'asset'   => AssetController::class,
            'cache'   => CacheController::class,
            'fixture' => FixtureController::class,
            'help'    => HelpController::class,
            'i18n'    => I18nController::class,
            'message' => MessageController::class,
            'migrate' => MigrateController::class,
            'serve'   => ServeController::class,
        ];
    }
}
