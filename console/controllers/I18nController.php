<?php

declare(strict_types=1);

namespace yii2kernel\console\controllers;

use Yii;
use yii\helpers\FileHelper;
use yii\i18n\DbMessageSource;
use yii2kernel\console\Controller;

class I18nController extends Controller
{
    public function actionUpdateDb()
    {
        Yii::$app->getI18n()->updateDb();
    }

    public function actionUpdateFiles()
    {
        Yii::$app->getI18n()->updateFiles();
    }
}
