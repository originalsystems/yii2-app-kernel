<?php

declare(strict_types=1);

class m180618_000000_file_remove_path extends \yii\db\Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE "public"."file"
DROP COLUMN "uri";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE "public"."deleted_file"
DROP COLUMN "uri";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE OR REPLACE FUNCTION delete_file() RETURNS TRIGGER AS
$$
BEGIN
    INSERT INTO "public"."deleted_file" ("content_type", "original_name", "path", "deleted_at")
    VALUES (OLD."content_type", OLD."original_name", OLD."path", NOW());

    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        $this->execute($sql);
    }

    public function safeDown()
    {
        $sql = <<<SQL
ALTER TABLE "public"."file"
ADD COLUMN "uri" VARCHAR(512) NULL;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE "public"."deleted_file"
ADD COLUMN "uri" VARCHAR(512) NULL;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE OR REPLACE FUNCTION delete_file() RETURNS TRIGGER AS
$$
BEGIN
    INSERT INTO "public"."deleted_file" ("content_type", "original_name", "path", "uri", "deleted_at")
    VALUES (OLD."content_type", OLD."original_name", OLD."path", OLD."uri", NOW());

    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        $this->execute($sql);
    }
}
