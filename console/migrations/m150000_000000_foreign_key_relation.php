<?php

declare(strict_types=1);

class m150000_000000_foreign_key_relation extends \yii\db\Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE VIEW "public"."foreign_key_relation" AS SELECT
    "table_constraints"."constraint_name"    AS "constraint_name",
    "table_constraints"."table_schema"       AS "table_schema",
    "table_constraints"."table_name"         AS "table_name",
    "key_column_usage"."column_name"         AS "column_name",
    "constraint_column_usage"."table_schema" AS "foreign_table_schema",
    "constraint_column_usage"."table_name"   AS "foreign_table_name",
    "constraint_column_usage"."column_name"  AS "foreign_column_name"
FROM
    "information_schema"."table_constraints"
    INNER JOIN "information_schema"."key_column_usage"
        ON "information_schema"."table_constraints"."constraint_name" = "information_schema"."key_column_usage"."constraint_name"
    INNER JOIN "information_schema"."constraint_column_usage"
        ON "information_schema"."constraint_column_usage"."constraint_name" = "information_schema"."table_constraints"."constraint_name"
WHERE "table_constraints"."constraint_type" = 'FOREIGN KEY';
SQL;

        $this->execute($sql);
    }

    public function safeDown()
    {
        $this->execute('DROP VIEW "public"."foreign_key_relation"');
    }
}
