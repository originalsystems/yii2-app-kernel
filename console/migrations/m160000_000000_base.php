<?php

declare(strict_types=1);

class m160000_000000_base extends \yii\db\Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE "public"."file" (
    "id"            SERIAL4      NOT NULL,
    "content_type"  VARCHAR(128) NOT NULL,
    "original_name" VARCHAR(255) NOT NULL,
    "path"          VARCHAR(512) NOT NULL,
    "uri"           VARCHAR(512) NULL,
    "created_at"    TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("id")
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "deleted_file" (
    "id"            SERIAL4      NOT NULL,
    "content_type"  VARCHAR(128) NOT NULL,
    "original_name" VARCHAR(255) NOT NULL,
    "path"          VARCHAR(512) NOT NULL,
    "uri"           VARCHAR(512) NULL,
    "deleted_at"    TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("id")
)
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE FUNCTION delete_file() RETURNS TRIGGER AS
$$
BEGIN
    INSERT INTO "public"."deleted_file" ("content_type", "original_name", "path", "uri", "deleted_at")
    VALUES (OLD."content_type", OLD."original_name", OLD."path", OLD."uri", NOW());
    
    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TRIGGER "delete_file" AFTER DELETE ON "public"."file"
FOR EACH ROW EXECUTE PROCEDURE delete_file();
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "public"."user" (
    "id"                   SERIAL4      NOT NULL,
    "image_id"             INT4         NULL,
    "status"               VARCHAR(24)  NOT NULL,
    "code"                 VARCHAR(12)  NOT NULL UNIQUE,
    "login"                VARCHAR(255) NOT NULL UNIQUE,
    "email"                VARCHAR(255) NOT NULL UNIQUE,
    "fields"               JSONB        NULL,
    "created_at"           TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    "updated_at"           TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("image_id") REFERENCES "public"."file" ("id") ON UPDATE CASCADE ON DELETE SET NULL
);
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "file_idx_content_type"          ON "public"."file"         USING BTREE ("content_type")');
        $this->execute('CREATE INDEX "deleted_file_idx_content_type"  ON "public"."deleted_file" USING BTREE ("content_type")');
        $this->execute('CREATE INDEX "user_idx_code"                  ON "public"."user"         USING BTREE ("code")');
        $this->execute('CREATE INDEX "user_idx_status"                ON "public"."user"         USING BTREE ("status")');
    }

    public function safeDown()
    {
        $this->execute('DROP FUNCTION delete_file CASCADE');

        $this->dropTable('"public"."user"');
        $this->dropTable('"public"."deleted_file"');
        $this->dropTable('"public"."file"');
    }
}
