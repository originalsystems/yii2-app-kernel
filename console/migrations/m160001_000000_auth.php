<?php

declare(strict_types=1);

class m160001_000000_auth extends \yii\db\Migration
{
    public function safeUp()
    {
        $this->execute('CREATE SCHEMA "auth";');

        $sql = <<<SQL
CREATE TABLE "auth"."rule" (
    "name"       VARCHAR(64) NOT NULL,
    "data"       TEXT        NULL,
    "created_at" INT4,
    "updated_at" INT4,
    PRIMARY KEY ("name")
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "auth"."item" (
    "name"        VARCHAR(64) NOT NULL,
    "type"        INTEGER     NOT NULL,
    "description" TEXT        NULL,
    "rule_name"   VARCHAR(64) NULL,
    "data"        TEXT        NULL,
    "created_at"  INT4,
    "updated_at"  INT4,
    PRIMARY KEY ("name"),
    FOREIGN KEY ("rule_name") REFERENCES "auth"."rule" ("name") ON UPDATE SET NULL ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "auth"."item_child" (
    "parent" VARCHAR(64) NOT NULL,
    "child"  VARCHAR(64) NOT NULL,
    PRIMARY KEY ("parent", "child"),
    FOREIGN KEY ("parent") REFERENCES "auth"."item" ("name") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("child")  REFERENCES "auth"."item" ("name") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "auth"."assignment" (
    "item_name"  VARCHAR(64) NOT NULL,
    "user_id"    INT4        NOT NULL,
    "created_at" INT4,
    PRIMARY KEY ("item_name", "user_id"),
    FOREIGN KEY ("item_name") REFERENCES "auth"."item" ("name") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("user_id")   REFERENCES "public"."user" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "item_idx_rule_name" ON "auth"."item" USING BTREE ("rule_name")');
    }

    public function safeDown()
    {
        $this->execute('DROP SCHEMA "auth" CASCADE');
    }
}
