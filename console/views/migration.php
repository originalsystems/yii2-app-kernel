<?php

/**
 * @var string $className the new migration class name without namespace
 * @var string $namespace the new migration class namespace
 **/

declare(strict_types=1);

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

declare(strict_types=1);

use yii\db\Migration;

class <?= $className; ?> extends Migration
{
    public function safeUp()
    {
    }

    public function safeDown()
    {
        return false;
    }
}
