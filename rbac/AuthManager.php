<?php

declare(strict_types=1);

namespace yii2kernel\rbac;

use yii\helpers\ArrayHelper;
use yii\rbac\Assignment;
use yii\rbac\DbManager;

class AuthManager extends DbManager
{
    protected $_assignments = [];

    public $itemTable       = '"auth"."item"';
    public $itemChildTable  = '"auth"."item_child"';
    public $assignmentTable = '"auth"."assignment"';
    public $ruleTable       = '"auth"."rule"';

    protected function storeAssignments($userID): void
    {
        if (array_key_exists($userID, $this->_assignments) === false) {
            $this->_assignments[$userID] = ArrayHelper::index(parent::getAssignments($userID), 'roleName');
        }
    }

    public function getAssignments($userID): array
    {
        $this->storeAssignments($userID);

        return $this->_assignments[$userID] ?? [];
    }

    public function getAssignment($roleName, $userID): ?Assignment
    {
        $this->storeAssignments($userID);

        return $this->_assignments[$userID][$roleName] ?? null;
    }
}
