<?php

declare(strict_types=1);

namespace yii2kernel\crud;

use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii2kernel\base\FormModel;
use yii2kernel\base\SearchModel;
use yii2kernel\db\ActiveQuery;
use yii2kernel\db\ActiveRecord;

class Controller extends \yii2kernel\web\Controller
{
    /**
     * @var string the model class name. This property must be set.
     */
    public $modelClass;

    /**
     * @var string the search class name. This property must be set.
     */
    public $searchClass;

    /**
     * @var string the form class name. This property must be set.
     */
    public $formClass;

    public $indexView = '@yii2kernel/crud/views/index';
    public $formView  = '@yii2kernel/crud/views/form';

    public function init(): void
    {
        parent::init();

        if ($this->modelClass === null || class_exists($this->modelClass) === false) {
            throw new InvalidConfigException('The "modelClass" must contain existent class name.');
        }

        if ($this->searchClass === null || class_exists($this->searchClass) === false) {
            throw new InvalidConfigException('The "searchClass" must contain existent class name.');
        }

        if ($this->formClass === null || class_exists($this->formClass) === false) {
            throw new InvalidConfigException('The "formClass" must contain existent class name.');
        }
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'index'  => ['GET', 'HEAD'],
                    'form'   => ['GET', 'POST', 'HEAD'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        /**
         * @var SearchModel $searchModel
         */
        $searchModel  = new $this->searchClass;
        $dataProvider = $searchModel->search(\Yii::$app->getRequest()->getQueryParams());

        return $this->render($this->indexView, [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm(int $id = null)
    {
        if ($id === null) {
            $model = new $this->modelClass();
        } else {
            $model = $this->findModel($id);
        }

        /**
         * @var FormModel $formModel
         */
        $formModel = new $this->formClass($model);

        $this->ajaxValidation($formModel);

        if ($formModel->load(\Yii::$app->getRequest()->post())) {
            if ($formModel->save()) {
                return $this->redirect(['form', 'id' => $model->getAttribute('id')]);
            }
        }

        return $this->render($this->formView, [
            'model'     => $model,
            'formModel' => $formModel,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     * @throws InvalidParamException
     */
    public function actionDelete(int $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function afterFindModel(ActiveQuery $query): void
    {
    }

    /**
     * @param integer $id
     *
     * @return \yii2kernel\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): \yii2kernel\db\ActiveRecord
    {
        /**
         * @var \yii2kernel\db\ActiveRecord $model
         * @var ActiveRecord                $class
         */
        $class = $this->modelClass;
        $query = $class::find();

        $this->afterFindModel($query);

        $query->andWhere(['id' => $id]);
        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException(\Yii::t('yii2kernel', 'The requested page does not exist.'));
        }

        return $model;
    }
}
