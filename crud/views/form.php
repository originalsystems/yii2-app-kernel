<?php

/**
 * @var \common\web\View               $this
 * @var \yii2kernel\db\ActiveRecord    $model
 * @var \yii2kernel\base\FormModel     $formModel
 * @var \yii2kernel\widgets\ActiveForm $form
 */

declare(strict_types=1);

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii2kernel\widgets\ActiveForm;

$this->title         = $model->getIsNewRecord() ? Yii::t('yii2kernel', 'Create') : $model::humanReadableName() . ' #' . $model->getPrimaryKey();
$this->breadcrumbs[] = ['label' => Inflector::pluralize($model::humanReadableName()), 'url' => ['index']];
$this->breadcrumbs[] = $this->title;

$fields = [];

foreach ($formModel->getValidators() as $validator) {
    foreach ($validator->attributes as $attribute) {
        switch (true) {
            case $validator instanceof \yii\validators\UrlValidator:
                $fields[$attribute] = ['input', ['url']];
                break;

            case $validator instanceof \yii\validators\EmailValidator:
                $fields[$attribute] = ['input', ['email']];
                break;

            case $validator instanceof \yii\validators\BooleanValidator:
                $fields[$attribute] = ['checkbox', []];
                break;

            case $validator instanceof \yii\validators\NumberValidator:
                $fields[$attribute] = ['input', ['number']];
                break;

            case $validator instanceof \yii\validators\EachValidator && $validator->rule[0] === 'in':
                $labels = [];
                $name   = lcfirst(Inflector::camelize($attribute)) . 'Labels';
                $data   = array_combine($validator->rule['range'], $validator->rule['range']);

                $reflection = new ReflectionClass($model::className());

                if ($reflection->hasProperty($name)) {
                    $property = $reflection->getProperty($name);

                    if ($property->isPublic() && $property->isStatic()) {
                        $labels = (array)$model::$$name;
                    }
                }

                if ($reflection->hasMethod($name)) {
                    $method = $reflection->getMethod($name);

                    if ($method->isPublic() && $method->isStatic()) {
                        $labels = (array)$model::$name();
                    }
                }

                foreach ($data as $value => $label) {
                    if (array_key_exists($value, $labels)) {
                        $data[$value] = $labels[$value];
                    }
                }

                $fields[$attribute] = [
                    'dropDownList',
                    [
                        $data,
                        [
                            'multiple' => true,
                            'prompt'   => Yii::t('yii2kernel', 'Specify one of values'),
                        ],
                    ],
                ];
                break;

            case $validator instanceof \yii\validators\RangeValidator:
                $labels = [];
                $name   = lcfirst(Inflector::camelize($attribute)) . 'Labels';
                $data   = array_combine($validator->range, $validator->range);

                $reflection = new ReflectionClass($model::className());

                if ($reflection->hasProperty($name)) {
                    $property = $reflection->getProperty($name);

                    if ($property->isPublic() && $property->isStatic()) {
                        $labels = (array)$model::$$name;
                    }
                }

                if ($reflection->hasMethod($name)) {
                    $method = $reflection->getMethod($name);

                    if ($method->isPublic() && $method->isStatic()) {
                        $labels = (array)$model::$name();
                    }
                }

                foreach ($data as $value => $label) {
                    if (array_key_exists($value, $labels)) {
                        $data[$value] = $labels[$value];
                    }
                }

                $fields[$attribute] = [
                    'dropDownList',
                    [
                        $data,
                        [
                            'prompt' => Yii::t('yii2kernel', 'Specify one of values'),
                        ],
                    ],
                ];
                break;

            case preg_match('#(password|secure|validate)#', $attribute) !== 0:
                $fields[$attribute] = ['input', ['password']];
                break;

            case preg_match('#(message|text|description)#', $attribute) !== 0:
                $fields[$attribute] = ['textarea', [['rows' => 4]]];
                break;

            default:
                if (array_key_exists($attribute, $fields) === false) {
                    $fields[$attribute] = [
                        'textInput',
                        [
                            [
                                'value' => is_array($formModel->{$attribute}) === false ? $formModel->{$attribute} : null,
                            ],
                        ],
                    ];
                }
        }
    }
}
?>
<section class="section-crud-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php foreach ($fields as $attribute => [$field, $arguments]): ?>
        <?= call_user_func_array([$form->field($formModel, $attribute), $field], $arguments); ?>
    <?php endforeach; ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton(\Yii::t('yii2kernel', 'Save'), ['class' => 'btn btn-success']); ?>
            <?= Html::a(Yii::t('yii2kernel', 'Cancel'), ['index'], ['class' => 'btn btn-info']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
