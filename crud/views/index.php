<?php

/**
 * @var \common\web\View                                         $this
 * @var \yii2kernel\base\SearchModel|\yii2kernel\db\ActiveRecord $searchModel
 * @var \yii\data\ActiveDataProvider                             $dataProvider
 */

declare(strict_types=1);

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Inflector;
use yii\widgets\Pjax;

$this->title         = Yii::t('yii2kernel', Inflector::pluralize($searchModel::humanReadableName()));
$this->breadcrumbs[] = $this->title;
?>
<section class="section-crud-index">
    <p>
        <?= Html::a(\Yii::t('yii2kernel', 'Create'), ['form'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => array_merge(array_slice($searchModel->attributes(), 0, 5), [
            [
                'class'    => \yii2kernel\grid\ActionColumn::class,
                'header'   => \Yii::t('yii2kernel', 'Actions'),
                'template' => '<div class="btn-group btn-group-sm">{form}{delete}</div>',
                'options'  => [
                    'width' => 80,
                ],
                'buttons'  => [
                    'form'   => function ($url) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'title'          => Yii::t('yii', 'Update'),
                            'aria-label'     => Yii::t('yii', 'Update'),
                            'class'          => 'btn btn-default tooltips',
                            'data-pjax'      => '0',
                            'data-placement' => 'bottom',
                        ]);
                    },
                    'delete' => function ($url) {
                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'title'          => Yii::t('yii', 'Delete'),
                            'aria-label'     => Yii::t('yii', 'Delete'),
                            'class'          => 'btn btn-danger tooltips',
                            'data-confirm'   => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'    => 'post',
                            'data-pjax'      => '0',
                            'data-placement' => 'bottom',
                        ]);
                    },
                ],
            ],
        ]),
    ]); ?>

    <?php Pjax::end(); ?>
</section>
